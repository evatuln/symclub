<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'BiodataController@tampilan');
// Route::get('/foto', 'GaleryController@foto');
Route::resource('galery', 'GaleryController');
Route::resource('biodata', 'BiodataController');
Route::get('/profile', 'ProfileController@profile');
Route::get('/visimisi', 'ProfileController@visimisi');
Route::get('/produkkami', 'ProfileController@produkkm');
Route::get('/kontakkami', 'ProfileController@kontakkm');
Route::get('/aboutus', 'ProfileController@aboutus');
Route::get('/home-artikel', 'ProfileController@artikel');
Route::get('/home-event', 'ProfileController@event');
Route::get('/home-galery', 'ProfileController@galery');
Route::get('/home-klienkm', 'ProfileController@klienkm');
Route::resource('artikel', 'ArtikelController');
Route::resource('event', 'EventController');





