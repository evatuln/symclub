<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use DB;
use Auth;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
         return view('event.create');
    }


    public function store(Request $request)
    {
            $request->validate([
                'nama_event' => 'required',
        ]);
       
        $reqEvent = $request->all();
        // $reqEvent['user_id']= Auth::id();
        Event::create($reqEvent);
        //dd($event);
        return redirect('/event')->with('success', 'Event Masuk Berhasil Disimpan!');
    }
    
    public function index()
    {
        $event = Event::all();
        return view('event.index', compact('event'));
    }

    public function show(Event $id)
    {
        $event = Event::find($id);
        return view('event.show', compact('event'));
    }

    public function edit(Event $id)
    {
        $event = Event::find($id);
            return view('event.edit', compact('event'));
    }


    public function update($id, Request $Request)
    {
        $reqEvent = $id->validate([
            'nama_event' => 'required',
        ]);

        // $reqEvent['user_id']= Auth::id();
        Event::where('id', $id)->update($reqEvent);
        //dd($event);

        return redirect('/event')->with('success', 'Berhasil Update EventS!');
    }

 
    public function destroy(Event $id)
    {
        Event::destroy($id);
            return redirect('/event')->with('success', 'Berhasil Update dihapus!');
    }
}
