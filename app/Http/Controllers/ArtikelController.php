<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use DB;
use Auth;

class ArtikelController extends Controller
{
  
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function create()
    {
         return view('artikel.create');
    }

    public function store(Request $request) 
    {
        $request->validate([
            'nama_artikel' => 'required',
            'tgl_artikel' => 'date',
            'isi_artikel' => 'required',
    ]);
   
    $reqArtikel = $request->all();
    // $reqArtikel['user_id']= Auth::id();
    Artikel::create($reqArtikel);
    //dd($artikel);
    return redirect('/artikel')->with('success', 'Artikel Masuk Berhasil Disimpan!');
    }

    
    public function index()
    {
        $artikel = Artikel::all();
        return view('artikel.index', compact('artikel'));
    }

    
    public function show(Artikel $id)
    {
        $artikel = Artikel::find($id);
        $data = array('artikel'=>$artikel);
        return view('artikel.show',$data);
        // $artikel = Artikel::find($id);
        // return view('artikel.show', compact('artikel'));
    }

    public function edit($id)
    {
        $artikel = Artikel::find($id);
            return view('artikel.edit', compact('artikel'));
    }

    public function update($id, Request $request)
    {
        $reqArtikel = $request->validate([ 
            'nama_artikel' => 'required|string|max:70',
            'tgl_artikel' => 'date',
            'isi_artikel' => 'required',
        ]);

        // $reqArtikel['user_id']= Auth::id();
        Artikel::where('id', $id)->update($reqArtikel);
        //dd($artikel);

        return redirect('/artikel')->with('success', 'Berhasil Update ArtikelS!');
    }

  
    public function destroy($id)
    {
        Artikel::destroy($id);
            return redirect('/artikel')->with('success', 'Berhasil Update dihapus!');
    }

    

}
