<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use App\Artikel;
Use App\Event;
Use App\Galery;

class ProfileController extends Controller
{
    public function profile()
    {
        return view('home.profile');
    }

    public function visimisi()
    {
        return view('home.visimisi');
    }

    public function produkkm()
    {
        return view('home.produkkm');
    }

    public function kontakkm()
    {
        return view('home.kontakkm');
    }

    public function aboutus()
    {
        return view('home.aboutus');
    }


    public function Artikel()
    {
        $artikel = Artikel::all();
        $data = array('artikel'=>$artikel);
        return view('home.artikel',$data);
    }

    

    public function Event()
    {
        $event = Event::all();
        $data = array('event'=>$event);
        return view('home.event',$data);
    }

    public function Galery()
        {
            $galery = galery::all();
            $data = array('galery'=>$galery);
            return view('home.galery',$data);
        }

    public function klienkm()
    {
        return view('home.klienkm');
    }


    public function tampilan()
        {
            return view('tampilan');
        }




}
