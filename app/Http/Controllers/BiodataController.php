<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Biodata;


class BiodataController extends Controller
{
   




    public function create()
    {
        $biodata = biodata::all();
        return view('biodata.index', compact('biodata'));
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jk' => 'required',
            'alamat' => 'required',
            'nohp' => ['required', 'digits:10'],
            'email' => 'required',
            'instagram' => 'required',
            'link_ig' => 'required',
            'facebook' => 'required',
            'link_fb' => 'required',
            'youtube' => 'required',
            'link_yb' => 'required',
        ]);
       
        $reqBio = $request->all();
        Biodata::create($reqBio);
        return redirect('/biodata')->with('success', 'Biodata Berhasil Disimpan!');
    }


    public function index()
    {
        $biodata = Biodata::all();
        return view('biodata.index', compact('biodata'));
    }


    public function edit($id)
        {
            $biodata = Biodata::find($id);
            return view('biodata.edit', compact('biodata'));
        }

        public function update($id, Request $request)
            {
                $reqBio = $request->validate([
                    'nama' => 'required',
                    'jk' => 'required',
                    'alamat' => 'required',
                    'nohp' => 'numeric',
                    'email' => 'required',
                    'instagram' => 'required',
                    'link_ig' => 'required',
                    'facebook' => 'required',
                    'link_fb' => 'required',
                    'youtube' => 'required',
                    'link_yb' => 'required',
                ]);

                Biodata::where('id', $id)->update($reqBio);

                return redirect('/biodata')->with('success', 'Berhasil Update Biodata!');
            }

            
        public function destroy($id)
        {
            Biodata::destroy($id);
            return redirect('/biodata')->with('success', 'Berhasil Update dihapus!');
        }

        public function tampilan()
        {
            $biodata = Biodata::find(1);
            $data = array('biodata'=>$biodata);
            return view('tampilan',$data);
        }

        
}
