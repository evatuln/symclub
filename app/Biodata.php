<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
protected $table = "biodata";

protected $fillable = [
    'nama', 'jk', 'alamat', 'nohp', 'email', 'instagram', 'facebook', 'youtube'
];
    
}