<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $table = "artikel";

    protected $fillable = 
    ['nama_artikel', 'tgl_artikel', 'isi_artikel'];
}
