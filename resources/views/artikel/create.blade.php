@extends('layout.master')

@section('title')
    Data Artikel
@endsection

@section('judul')
    Tambah Artikel
@endsection


@section('content')
    <div>
        <form action="/artikel" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Artikel</label>
                <input type="text" class="form-control" name="nama_artikel" id="title"
                    placeholder="Masukkan Nama Artikel">
                @error('nama_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Tanggal Artikel</label>
                <input type="date" class="form-control" name="tgl_artikel" id="title"
                    placeholder="Masukkan Tanggal Artikel">
                @error('tgl_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Isi Artikel</label>
                <input type="text" class="form-control" name="isi_artikel" id="title"
                    placeholder="Masukkan Isi Artikel">
                @error('isi_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
    </div>
@endsection
