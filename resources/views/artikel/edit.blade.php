@extends('layout.master')

@section('title')
    Data Artikel
@endsection

@section('judul')
    Edit Data dengan ID : {{ $artikel->id }}
@endsection

@section('content')
    <div>
        <form action="/artikel/{{ $artikel->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Artikel</label>
                <input type="text" class="form-control" value="{{ $artikel->nama_artikel }}" name="nama_artikel"
                    id="title" placeholder="Ketik Artikel">
                @error('nama_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Tanggal Artikel</label>
                <input type="date" class="form-control" value="{{ $artikel->tgl_artikel }}" name="tgl_artikel"
                    id="title" placeholder="Masukkan Tanggal artikel">
                @error('tgl_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Isi Artikel</label>
                <input type="text" class="form-control" value="{{ $artikel->isi_artikel }}" name="isi_artikel"
                    id="title" placeholder="Masukkan isi_artikel">
                @error('isi_artikel')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
    </div>
@endsection
