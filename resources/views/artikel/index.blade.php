@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data ARTIKEL
@endsection

@section('judul')
    <a href="{{ route('artikel.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i>
        Tambah</a>
@endsection


@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Artikel</th>
                <th scope="col">Tanggal Artikel</th>
                <th scope="col">Isi Artikel</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($artikel as $key=>$artikel)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $artikel->nama_artikel }}</td>
                    <td>{{ $artikel->tgl_artikel }}</td>
                    <td>{{ $artikel->isi_artikel }}</td>
                    <td style="display: flex;">
                        <form action="/artikel/{{ $artikel->id }}" method="POST">
                            <a href="/artikel/{{ $artikel->id }}/edit" class="btn btn-primary">Edit</a>

                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
