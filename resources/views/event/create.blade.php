@extends('layout.master')

@section('title')
    Data Event
@endsection

@section('judul')
    Tambah Event
@endsection


@section('content')
    <div>
        <form action="/event" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Event</label>
                <input type="text" class="form-control" name="nama_event" id="title" placeholder="Masukkan Nama Event">
                @error('nama_event')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
    </div>
@endsection
