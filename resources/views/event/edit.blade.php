@extends('layout.master')

@section('title')
    Data Event
@endsection

@section('judul')
    Edit Data dengan ID : {{ $event->id }}
@endsection

@section('content')
    <div>
        <form action="/event/{{ $event->id }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Event</label>
                <input type="text" class="form-control" value="{{ $event->nama_event }}" name="nama_event" id="title"
                    placeholder="Ketik Event">
                @error('nama_event')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
    </div>
@endsection
