@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data Event
@endsection

@section('judul')
    <a href="{{ route('event.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i>
        Tambah</a>
@endsection


@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Event</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($event as $key=>$event)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $event->nama_event }}</td>
                    <td style="display: flex;">
                        <form action="/event/{{ $event->id }}" method="POST">
                            {{-- <a href="/event/{{ $event->id }}/edit" class="btn btn-primary">Edit</a> --}}

                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
