@extends('tampilan')

@section('content')
    <div class="card-group">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Email</h5>
                <p class="card-text">customercare@mforce.co.id</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Last updated 3 mins ago</small>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Call</h5>
                <p class="card-text">(021) 89982862</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Last updated 3 mins ago</small>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Instagram</h5>
                <p class="card-text">sym.Indonesia
                <p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Last updated 3 mins ago</small>
            </div>
        </div>
    </div>
@endsection
