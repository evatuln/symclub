@extends('tampilan')

@section('content')
    <div class="card text-center">
        @forelse ($event as $key=>$event)
            <div class="btn btn-primary">
                {{ $event->nama_event }}
            </div>
            <div class="card-footer text-muted">
                <br>
            </div>
        @empty
        @endforelse
    </div>
@endsection
