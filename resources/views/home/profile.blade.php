@extends('tampilan')

@section('content')
    <div class="card mb-3">
        <img src="{{ asset('/page/plugins/images/misi.jpg') }}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Profile</h5>
            <p class="card-text">PT. MFORCE INDONESIA adalah sebuah perusahaan PMA dari MFORCE Bike Holding Sdn.Bhd, yang
                bergerak di bidang otomotif roda dua, yang merupakan distributor utama atau Agen Tunggal Pemegang Merk
                (ATPM) untuk merk sepeda motor SM SPORT & SYM.
                PT. MFORCE INDONESIA didirikan pada bulan November 2012, yang berlokasi di kawasan Industri MM2100 Cikarang
                Barat - Bekasi, Indonesia.<br> <br>
                Jenis Perusahaan : Perseroan Terbatas<br>
                Status Investasi : PMA (Penanaman Modal Asing)<br>
                Tanggal Pendirian : 02 November 2012<br>
                Aktivitas : Agen Tunggal Pemegang Merek (ATPM)<br>
                Jam Kerja Kantor : Senin - Kamis (08.00 – 17.00 WIB), Jumat (08.00 - 17:30 WIB)<br>
            </p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div>
    </div>
@endsection
