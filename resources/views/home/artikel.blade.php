@extends('tampilan')

@section('content')
    <div class="card text-center">
        @forelse ($artikel as $key=>$artikel)
            <div class="btn btn-primary">
                {{ $artikel->nama_artikel }}
            </div>
            <div class="card-body">
                <h5 class="card-title">{{ $artikel->isi_artikel }}</h5>
            </div>

            <div class="card-footer text-muted">
                {{ $artikel->tgl_artikel }}
            </div>
            <div class="card-footer text-muted">
                <br>
            </div>
        @empty
        @endforelse
    </div>
@endsection
