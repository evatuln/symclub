@extends('tampilan')

@section('content')
    <div class="card mb-3" style="max-width: 700px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/page/plugins/images/1.png') }}" style="width: 100%;" class="img-fluid rounded-start"
                    alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">SYM</h5>
                    <p class="card-text">Motor matic yg sangat Ciamik dan Unggul dan cocok untuk anda yg duka bergaya.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 700px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/page/plugins/images/2.png') }}" style="width: 100%;" class="img-fluid rounded-start"
                    alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">SM SPORT</h5>
                    <p class="card-text">Kendaraan yang sangat Sporty untuk berolah raga.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 700px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/page/plugins/images/3.png') }}" style="width: 100%;" class="img-fluid rounded-start"
                    alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">CF MOTO</h5>
                    <p class="card-text">Motor Lakik untuk menjemput pacar dikala malam minggu juga oke</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>

    <div class="card mb-3" style="max-width: 700px;">
        <div class="row g-0">
            <div class="col-md-4">
                <img src="{{ asset('/page/plugins/images/4.png') }}" style="width: 100%;" class="img-fluid rounded-start"
                    alt="">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">WMOTO</h5>
                    <p class="card-text">Motor Lucuk.</p>
                    <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
@endsection
