@extends('tampilan')

@section('content')
    <div class="row row-cols-1 row-cols-md-2 g-4">
        <div class="col">
            <div class="card">
                <img src="{{ asset('/page/plugins/images/k.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Honda</h5>
                    <p class="card-text">Salah satu klien kami adalah Honda. Karena mereka lagi Inden jadi mengambil barang
                        dari kami.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <img src="{{ asset('/page/plugins/images/l.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">SMK MOTOR</h5>
                    <p class="card-text">Kami Juga melakukan pengecekan berkala dan belahar bersama anak2 SMK.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <img src="{{ asset('/page/plugins/images/m.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Klinik</h5>
                    <p class="card-text">Kendaraan kami bekerjasama dengan Klinik yang menguji performa Kendaraan kita dan
                        Oli kita.</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <img src="{{ asset('/page/plugins/images/n.jpg') }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Rossie</h5>
                    <p class="card-text">Rosi menggunakkan barang dari produk kita.
                </div>
            </div>
        </div>
    @endsection
