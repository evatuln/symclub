@extends('tampilan')

@section('content')
    <div class="card mb-3">
        <img src="{{ asset('/page/plugins/images/sym.jpg') }}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title"></h5>
            <p class="card-text">
                DAYA TAHAN<br>
                Memiliki kekuatan internal untuk bertahan dari budaya kita diterjemahkan menjadi komitmen terhadap keahlian
                profesional, produk yang hidup lebih lama, dan perusahaan yang melakukan dan bersaing untuk jangka panjang.
                <br>
                <br>
                DAYA TARIK<br>
                Daya tarik perusahaan dan produk kami akan menarik orang-orang yang memiliki nilai yang sama, menciptakan
                hubungan berdasarkan pemahaman pelanggan, mitra, dan karyawan kami yang hanya bisa hadir karena kami
                menyukai mereka. Kami percaya apa yang mereka percaya.<br>
            </p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div>
    </div>
@endsection
