@extends('tampilan')

@section('content')
    <div class="card">
        <div class="card-header">
            VISI
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>PT Mforce Indonesia menjadi perusahaan yang bergerak di bidang otomotif yang sukses dan bereputasi baik
                    di Indonesia, meningkatkan kualitas kerja dan profesionalisme kerja untuk menghasilkan produk yang
                    berkualitas.</p>
                <footer class="blockquote-footer">SYM<cite title="Source Title">Club</cite></footer>
            </blockquote>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="card-header">
            Misi
        </div>
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>Menyediakan produk dan layanan unggulan, memenangkan loyalitas pelanggan, dan memenuhi tanggung jawab
                    sosial..</p>
                <footer class="blockquote-footer">SYM <cite title="Source Title">Club</cite></footer>
            </blockquote>
        </div>
    </div>
@endsection
