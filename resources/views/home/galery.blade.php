@extends('tampilan')

@section('content')
    <div class="row row-cols-1 row-cols-md-2 g-4">
        @forelse ($galery as $data)
            <div class="col">
                <div class="card">
                    <img src="{{ URL::asset('img/' . $data->image) }}" class="card-img-top" alt="...">
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="..." class="card-img-top" alt="...">
                </div>
            </div>
        @empty
        @endforelse
    </div>
@endsection
