<div class="sidebar">
    <!-- Sidebar user (optional) -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/biodata" class="nav-link">
                    <i class="nav-icon fas fa-user-tie"></i>
                    <p>
                        User
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/artikel" class="nav-link">
                    <i class="nav-icon fa fa-inbox"></i>
                    <p>
                        Artikel
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/event" class="nav-link">
                    <i class="nav-icon fa fa-inbox"></i>
                    <p>
                        Event
                    </p>
                </a>
            </li>

            <li class="nav-item">
                <a href="/galery" class="nav-link">
                    <i class="nav-icon far fa-image"></i>
                    <p>
                        Galery
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/home-klienkm" class="nav-link">
                    <i class="nav-icon fa fa-inbox"></i>
                    <p>
                        Klien Kami
                    </p>
                </a>
            </li>

    </nav>
    <!-- /.sidebar-menu -->
</div>
