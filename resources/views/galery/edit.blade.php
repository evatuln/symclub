@extends('layout.master')

@section('content')
    <div class="ml-3 mt-3 mr-3">
        <h2>Edit galery {{ $galery->id }}</h2>
        <form action="/galery/{{ $galery->id }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            {{-- //method put berfungsi sebagai edit --}}
            <div class="form-group">
                <label for="image">Foto</label>
                <input type="file" class="form-control" name="image" id="image" value="{{ $galery->image }}"
                    placeholder="Masukkan Foto">
                @error('image')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection
