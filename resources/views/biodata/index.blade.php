@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i> Data User
@endsection

@section('judul')
@endsection


@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Jenis Kelamin</th>
                <th scope="col">Alamat</th>
                <th scope="col">No Hp</th>
                <th scope="col">Email</th>
                <th scope="col">Instagram</th>
                <th scope="col">Facebook</th>
                <th scope="col">Youtube</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($biodata as $key=>$biodata)
                <tr>
                    <td>{{ $key + 1 }}</th>
                    <td>{{ $biodata->nama }}</td>
                    <td>{{ $biodata->jk }}</td>
                    <td>{{ $biodata->alamat }}</td>
                    <td>{{ $biodata->nohp }}</td>
                    <td>{{ $biodata->email }}</td>
                    <td>{{ $biodata->instagram }}</td>
                    <td>{{ $biodata->facebook }}</td>
                    <td>{{ $biodata->youtube }}</td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
